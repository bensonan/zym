Zym Framework
-------------

Zym Framework is a set of libraries and bundles that extend Symfony [http://symfony.com] 
to simplify the development process.
