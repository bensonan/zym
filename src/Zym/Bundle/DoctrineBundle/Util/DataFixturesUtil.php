<?php

namespace Zym\Bundle\DoctrineBundle\Util;

use Doctrine\DBAL\Driver\PDOMySql\Driver as MySqlDriver;
use Doctrine\ORM\EntityManagerInterface;

class DataFixturesUtil
{
    /**
     * @param EntityManagerInterface $em
     * @param string $class
     * @return void
     */
    public static function resetAutoIncrementByEntityClass(EntityManagerInterface $em, $class)
    {
        $connection = $em->getConnection();

        if ($connection->getDriver() instanceof MySqlDriver) {
            $tableName = $em->getClassMetadata($class)->getTableName();
            $connection->exec(sprintf('ALTER TABLE `%s` AUTO_INCREMENT = 1;', $tableName));
        }
    }
}
