<?php

/**
 * Zym Framework
 *
 * This file is part of the Zym package.
 *
 * @link      https://github.com/geoffreytran/zym for the canonical source repository
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3 License
 */

namespace Zym\Bundle\ThemeBundle\Controller;

use JMS\SecurityExtraBundle\Annotation\Secure;
use JMS\SecurityExtraBundle\Annotation\SecureParam;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Zym\Bundle\ThemeBundle\Entity\ThemeRule;
use Zym\Bundle\ThemeBundle\Entity\ThemeRuleManager;
use Zym\Bundle\ThemeBundle\Form\DeleteType;
use Zym\Bundle\ThemeBundle\Form\ThemeRuleType;

/**
 * Class ThemeRulesController
 *
 * @package Zym\Bundle\ThemeBundle\Controller
 * @author  Geoffrey Tran <geoffrey.tran@gmail.com>
 */
class ThemeRulesController extends Controller
{
    /**
     * @Route(
     *     "", name="zym_theme_theme_rules")
     * @Template()
     */
    public function listAction(Request $request)
    {
        $page     = $request->query->get('page', 1);
        $limit    = $request->query->get('limit', 50);
        $orderBy  = $request->query->get('orderBy', []);
        $filterBy = $request->query->get('filterBy', []);

        $themeRuleManager = $this->get('zym_theme.theme_rule_manager');
        $themeRules = $themeRuleManager->findThemeRules($filterBy, $page, $limit, $orderBy);

        return [
            'themeRules' => $themeRules,
        ];
    }

    /**
     * @Route("/add", name="zym_theme_theme_rules_add")
     * @Template()
     */
    public function addAction(Request $request)
    {
        $authChecker = $this->get('security.authorization_checker');

        if (!$authChecker->isGranted('OPERATOR', new ObjectIdentity('class', ThemeRule::class))) {
            throw new AccessDeniedException();
        }

        $themeRule = new ThemeRule();
        $form = $this->createForm(ThemeRuleType::class, $themeRule);

        if ($request->isMethod(Request::METHOD_POST)) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $themeRuleManager = $this->get('zym_theme.theme_rule_manager');
                $themeRuleManager->createThemeRule($themeRule);

                $translator = $this->get('translator');

                $request->getSession()->setFlash($translator->trans('Created the new theme rule successfully.'), 'success');

                return $this->redirectToRoute('zym_theme_theme_rules');
            }
        }

        return [
            'form' => $form->createView(),
        ];
    }

    /**
     * @Route("/{id}/edit", name="zym_theme_theme_rules_edit")
     * @Template()
     * @SecureParam(name="themeRule", permissions="EDIT")
     */
    public function editAction(Request $request, ThemeRule $themeRule)
    {
        $origThemeRule = clone $themeRule;
        $form = $this->createForm(ThemeRuleType::class, $themeRule);

        if ($request->isMethod(Request::METHOD_POST)) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $themeRuleManager = $this->get('zym_theme.theme_rule_manager');
                $themeRuleManager->saveThemeRule($themeRule);

                $translator = $this->get('translator');

                $request->getSession()->getFlashBag()->add('success', $translator->trans('Changes saved!'));

                return $this->redirectToRoute('zym_theme_theme_rules');
            }
        }

        return [
            'themeRule' => $origThemeRule,
            'form'      => $form->createView(),
        ];
    }

    /**
     * Delete an theme rule
     *
     *
     * @Route(
     *     "/{id}",
     *     requirements={},
     *     methods={"DELETE"}
     * )
     *
     * @Route(
     *     "/{id}/delete.{_format}",
     *     name="zym_theme_theme_rules_delete",
     *     defaults = {
     *         "_format" = "html"
     *     },
     *     requirements = {
     *         "_format" = "html|json|ajax"
     *     }
     * )
     *
     * @Template()
     *
     * @SecureParam(name="themeRule", permissions="DELETE")
     */
    public function deleteAction(Request $request, ThemeRule $themeRule)
    {
        $origThemeRule = clone $themeRule;

        /** @var ThemeRuleManager $themeRuleManager */
        $themeRuleManager = $this->get('zym_theme.theme_rule_manager');

        $form = $this->createForm(DeleteType::class, $themeRule);

        if ($request->isMethod(Request::METHOD_POST)) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $themeRuleManager->deleteThemeRule($themeRule);

                $translator = $this->get('translator');

                $request->getSession()->setFlash($translator->trans('Theme rule deleted.'), 'success');

                return $this->redirectToRoute('zym_theme_theme_rules');
            }
        }

        if ($request->isMethod(Request::METHOD_DELETE)) {
            $themeRuleManager->deleteThemeRule($themeRule);
            return $this->redirectToRoute('zym_theme_theme_rules');
        }

        return [
            'themeRule' => $origThemeRule,
            'form'      => $form->createView(),
        ];
    }
}
