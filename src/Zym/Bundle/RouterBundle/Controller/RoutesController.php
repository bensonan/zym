<?php

/**
 * Zym Framework
 *
 * This file is part of the Zym package.
 *
 * @link      https://github.com/geoffreytran/zym for the canonical source repository
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3 License
 */

namespace Zym\Bundle\RouterBundle\Controller;

use JMS\SecurityExtraBundle\Annotation\Secure;
use JMS\SecurityExtraBundle\Annotation\SecureParam;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Zym\Bundle\RouterBundle\Entity\Route as RouteEntity;
use Zym\Bundle\RouterBundle\Entity\RouteManager;
use Zym\Bundle\RouterBundle\Form\DeleteType;
use Zym\Bundle\RouterBundle\Form\RouteType;

/**
 * Routes Controller
 *
 * @author    Geoffrey Tran
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 */
class RoutesController extends Controller
{
    /**
     * @Route(
     *     ".{_format}",
     *     name="zym_router_routes",
     *     defaults = { "_format" = "html" }
     * )
     * @Template()
     *
     * @param Request $request
     */
    public function listAction(Request $request)
    {
        $page     = $request->query->get('page', 1);
        $limit    = $request->query->get('limit', 50);
        $orderBy  = $request->query->get('orderBy');
        $filterBy = $request->query->get('filterBy');

        /** @var RouteManager $routeManager */
        $routeManager  = $this->get('zym_router.route_manager');
        $routes        = $routeManager->findRoutes($filterBy, $page, $limit, $orderBy);

        return [
            'routes' => $routes,
        ];
    }

    /**
     * @Route("/add", name="zym_router_routes_add")
     * @Template()
     *
     * @param Request $request
     */
    public function addAction(Request $request)
    {
        $authChecker = $this->get('security.authorization_checker');

        if (!$authChecker->isGranted('CREATE', new ObjectIdentity('class', RouteEntity::class))) {
            throw new AccessDeniedException();
        }

        $route = new RouteEntity(null, null);
        $form = $this->createForm(RouteType::class, $route);

        if ($request->isMethod(Request::METHOD_POST)) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                /** @var RouteManager $routeManager */
                $routeManager = $this->get('zym_router.route_manager');
                $routeManager->createRoute($route);

                return $this->redirectToRoute('zym_router_routes');
            }
        }

        return [
            'form' => $form->createView(),
        ];
    }

    /**
     * Edit a route
     *
     * @param Route $route
     *
     * @Route("/{name}/edit", name="zym_router_routes_edit")
     * @ParamConverter("route", class="ZymRouterBundle:Route")
     * @Template()
     *
     * @SecureParam(name="route", permissions="EDIT")
     */
    public function editAction(Request $request, Route $route)
    {
        $origRoute = clone $route;
        $form = $this->createForm(RouteType::class, $route);

        if ($request->isMethod(Request::METHOD_POST)) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                /** @var RouteManager $routeManager */
                $routeManager = $this->get('zym_router.route_manager');
                $routeManager->saveRoute($route);

                return $this->redirectToRoute('zym_router_routes');
            }
        }

        return [
            'route' => $origRoute,
            'form' => $form->createView(),
        ];
    }

    /**
     * Delete a route
     *
     * @Route(
     *     "/{name}",
     *     requirements={},
     *     methods={"DELETE"}
     * )
     * @Route(
     *     "/{name}/delete.{_format}",
     *     name="zym_router_routes_delete",
     *     defaults={
     *         "_format" = "html"
     *     }
     * )
     *
     * @Template()
     *
     * @SecureParam(name="route", permissions="DELETE")
     *
     * @param Request $request
     * @param Route   $route
     */
    public function deleteAction(Request $request, Route $route)
    {
        $origRoute = clone $route;

        /** @var RouteManager $routeManager */
        $routeManager = $this->get('zym_router.route_manager');
        $form = $this->createForm(DeleteType::class, $route);

        if ($request->isMethod(Request::METHOD_POST)) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $routeManager->deleteRoute($route);

                return $this->redirectToRoute('zym_router_routes');
            }
        }

        if ($request->isMethod(Request::METHOD_DELETE)) {
            $routeManager->deleteRoute($route);

            return $this->redirectToRoute('zym_router_routes');
        }

        return [
            'route' => $origRoute,
            'form' => $form->createView(),
        ];
    }

    /**
     * Show a route
     *
     * @Route(
     *     "/{name}.{_format}",
     *     name     ="zym_router_routes_show",
     *     defaults = { "_format" = "html" }
     * )
     * @ParamConverter("route", class="ZymRouterBundle:Route")
     * @Template()
     *
     * @SecureParam(name="route", permissions="VIEW")
     *
     * @param Route $route
     */
    public function showAction(Route $route)
    {
        return [
            'route' => $route,
        ];
    }
}
