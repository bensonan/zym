<?php

/**
 * Zym Framework
 *
 * This file is part of the Zym package.
 *
 * @link      https://github.com/geoffreytran/zym for the canonical source repository
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3 License
 */

namespace Zym\Bundle\UserBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\ChoiceList\ChoiceList;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TimezoneType;
use Symfony\Component\Form\FormBuilderInterface;
use Zym\Bundle\SecurityBundle\Form\AclSecurityIdentityEntityType;
use Zym\Bundle\UserBundle\Model\SingleNameInterface;
use Zym\Bundle\UserBundle\Model\SplitNameInterface;
use Zym\Bundle\UserBundle\Model\TimeZoneInterface;

/**
 * Class UserType
 *
 * @package Zym\Bundle\UserBundle\Form
 * @author  Geoffrey Tran <geoffrey.tran@gmail.com>
 */
class UserType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if ($options['data'] instanceof SplitNameInterface) {
            $builder
                ->add('firstName', TextType::class, [
                    'label' => 'First Name',
                ])
                ->add('middleName', TextType::class, [
                    'label'    => 'Middle Name',
                    'required' => false,
                ])
                ->add('lastName', TextType::class, [
                    'label' => 'Last Name',
                ])
            ;
        } else if ($options['data'] instanceof SingleNameInterface) {
            $builder->add('name', TextType::class, [
                'label' => 'Name',
            ]);
        }

        if ($options['data']->getUsernameCanonical() !== $options['data']->getEmailCanonical()) {
            $builder->add('username');
        }

        $builder
            ->add('email', EmailType::class)
            ->add('plainPassword', RepeatedType::class, [
                'type'            => PasswordType::class,
                'first_name'      =>'password',
                'second_name'     =>'confirmPassword',
                'second_options'  => ['label' => 'Confirm Password'],
                'error_bubbling'  => true,
                'invalid_message' => 'Passwords do not match',
            ])
            ->add('roles', AclSecurityIdentityEntityType::class, [
                'attr' => [
                    'placeholder' => 'Choose your roles.',
                ],
            ])
            ->add('groups', EntityType::class, [
                'class'    => 'ZymUserBundle:Group',
                'property' => 'name',
                'multiple' => true,
                'required' => false,
                'attr'     => [
                    'placeholder' => 'Choose your groups.',
                ],
            ])
        ;

        if ($options['data'] instanceof TimeZoneInterface) {
            $builder
                ->add('timeZone', TimezoneType::class, [
                    'label'       => 'Time Zone',
                    'empty_value' => 'Choose your time zone.',
                    'empty_data'  => null,
                    'required'    => false,
                ])
            ;
        }

        $builder
            ->add('enabled', CheckboxType::class, [
                'label'       => 'Enabled',
                'help_block'  => 'Whether user is enabled.',
                'help_widget_popover' => [
                    'title' => 'help popover text',
                    'content' => 'beautiful, isn\'t it?',
                ],
                'required' => false,
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return '';
    }
}
