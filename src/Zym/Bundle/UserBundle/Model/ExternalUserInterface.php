<?php

namespace Zym\Bundle\UserBundle\Model;

/**
 * Interface ExternalUserInterface
 *
 * For identifying users by external applications.
 *
 * @package Zym\Bundle\UserBundle\Model
 * @author  Paul Harvey <pharvpro@gmail.com>
 */
interface ExternalUserInterface
{
    /**
     * Get the user's external ID, which is ideally a GUID to avoid collisions with other user providers.
     *
     * @return string
     */
    public function getExtId();
}
