<?php

/**
 * Zym Framework
 *
 * This file is part of the Zym package.
 *
 * @link      https://github.com/geoffreytran/zym for the canonical source repository
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3 License
 */

namespace Zym\Bundle\UserBundle\Entity;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\Persistence\ObjectRepository;
use FOS\UserBundle\Doctrine\GroupManager as AbstractGroupManager;
use FOS\UserBundle\Model\GroupInterface;
use FOS\UserBundle\Util\CanonicalizerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\Security\Acl\Domain\AclCollectionCache;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Acl\Exception\AclAlreadyExistsException;
use Symfony\Component\Security\Acl\Exception\AclNotFoundException;
use Symfony\Component\Security\Acl\Exception\NotAllAclsFoundException;
use Symfony\Component\Security\Acl\Model\MutableAclProviderInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Zym\Bundle\FieldBundle\Entity\FieldManager;
use Zym\Bundle\FrameworkBundle\Model\PageableRepositoryInterface;

/**
 * Class GroupManager
 *
 * @package Zym\Bundle\UserBundle\Entity
 * @author  Geoffrey Tran <geoffrey.tran@gmail.com>
 */
class GroupManager extends AbstractGroupManager
{
    /**
     * Acl Provider
     *
     * @var MutableAclInterface
     */
    private $aclProvider;

    /**
     * Acl Collection Cache
     *
     * @var AclCollectionCache
     */
    private $aclCollectionCache;

    /**
     * Security Token Storage
     *
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * Constructor.
     *
     * @param ObjectManager               $om
     * @param string                      $class
     * @param PaginatorInterface          $paginator
     * @param MutableAclProviderInterface $aclProvider
     * @param TokenStorageInterface       $tokenStorage
     * @param AclCollectionCache          $aclCollectionCache
     */
    public function __construct(ObjectManager               $om,
                                $class,
                                PaginatorInterface          $paginator,
                                MutableAclProviderInterface $aclProvider,
                                TokenStorageInterface       $tokenStorage = null,
                                AclCollectionCache          $aclCollectionCache = null)
    {
        parent::__construct($om, $class);

        $this->setRepository($om->getRepository($class));

        $metadata    = $om->getClassMetadata($class);
        $this->class = $metadata->name;

        if ($this->getRepository() instanceof PageableRepositoryInterface) {
            $this->getRepository()->setPaginator($paginator);
        }

        $this->setAclProvider($aclProvider);

        if ($tokenStorage) {
            $this->setSecurityTokenStorage($tokenStorage);
        }

        if ($aclCollectionCache) {
            $this->setAclCollectionCache($aclCollectionCache);
        }
    }

    /**
     * Create a Group
     *
     * @param GroupInterface $group
     * @return GroupInterface
     */
    public function addGroup(GroupInterface $group)
    {
        $this->createEntity($group);

        return $group;
    }

    /**
     * Save a Group
     *
     * @param GroupInterface $group
     * @param bool $andFlush
     */
    public function saveGroup(GroupInterface $group, $andFlush = true)
    {
        $this->saveEntity($group, $andFlush);
    }

    /**
     * Delete a Group
     *
     * @param GroupInterface $group
     */
    public function deleteGroup(GroupInterface $group)
    {
        $this->deleteEntity($group);
    }

    /**
     * Find Groups
     *
     * @param array $criteria
     * @param integer $page
     * @param integer $limit
     * @param array $orderBy
     * @return Paginator
     */
    public function findGroups(array $criteria = null, $page = 1, $limit = 50, array $orderBy = null)
    {
        $entities = $this->repository->findGroups($criteria, $page, $limit, $orderBy);
        $this->loadAcls($entities);
        return $entities;
    }

    /**
     * Find a Group by criteria
     *
     * @param array $criteria
     * @return Group
     */
    public function findGroupBy(array $criteria)
    {
        return $this->repository->findOneBy($criteria);
    }

    /**
     * Find a node by criteria
     *
     * @param array $criteria
     * @return GroupManager
     */
    public function findOneBy(array $criteria)
    {
        return $this->repository->findOneBy($criteria);
    }

    /**
     *
     * @return ObjectRepository
     */
    public function getRepository()
    {
        return $this->repository;
    }

    /**
     * Set the repository
     *
     * @param ObjectRepository $repository
     * @return GroupManager
     */
    protected function setRepository(ObjectRepository $repository)
    {
        $this->repository = $repository;
        return $this;
    }

    /**
     * Get the entity class
     *
     * @return string
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * Get the acl provider
     *
     * @return MutableAclProviderInterface
     */
    public function getAclProvider()
    {
        return $this->aclProvider;
    }

    /**
     * Set the acl provider
     *
     * @param MutableAclProviderInterface $aclProvider
     * @return GroupManager
     */
    public function setAclProvider(MutableAclProviderInterface $aclProvider)
    {
        $this->aclProvider = $aclProvider;
        return $this;
    }

    /**
     * Get the acl collection cache
     *
     * @return AclCollectionCache
     */
    public function getAclCollectionCache()
    {
        return $this->aclCollectionCache;
    }

    /**
     * Set the acl collection cache
     *
     * @param AclCollectionCache $aclCollectionCache
     * @return AbstractEntityManager
     */
    public function setAclCollectionCache(AclCollectionCache $aclCollectionCache)
    {
        $this->aclCollectionCache = $aclCollectionCache;
        return $this;
    }

    /**
     * Get the security token storage
     *
     * @return TokenStorageInterface
     */
    public function getSecurityTokenStorage()
    {
        return $this->tokenStorage;
    }

    /**
     * Set the security token storage
     *
     * @param TokenStorageInterface $tokenStorage
     * @return AbstractEntityManager
     */
    public function setSecurityTokenStorage(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
        return $this;
    }

    /**
     * Create an entity
     *
     * @param object $entity
     * @return object
     */
    protected function createEntity($entity)
    {
        $this->objectManager->beginTransaction();

        try {
            $this->objectManager->persist($entity);
            $this->objectManager->flush();

            // Acl
            $aclProvider = $this->aclProvider;
            $oid         = ObjectIdentity::fromDomainObject($entity);

            try {
                $acl = $aclProvider->createAcl($oid);
                $aclProvider->updateAcl($acl);
            } catch (AclAlreadyExistsException $e) {

            }

            $this->objectManager->commit();
        } catch (\Exception $e) {
            $this->objectManager->rollback();
            throw $e;
        }

        return $entity;
    }

    /**
     * Delete an entity
     *
     * @param object $entity
     * @return object
     */
    protected function deleteEntity($entity)
    {
        $this->objectManager->beginTransaction();

        try {
            // Acl
            $aclProvider = $this->aclProvider;
            $oid         = ObjectIdentity::fromDomainObject($entity);
            $acl         = $aclProvider->deleteAcl($oid);

            $this->objectManager->remove($entity);
            $this->objectManager->flush();

            $this->objectManager->commit();
        } catch (\Exception $e) {
            $this->objectManager->rollback();
            throw $e;
        }

        return $entity;
    }

    /**
     * Save an entity
     *
     * @param object $node
     * @param bool $andFlush
     */
    protected function saveEntity($entity, $andFlush = true)
    {
        $this->objectManager->persist($entity);

        if ($andFlush) {
            $this->objectManager->flush();
        }

        return $entity;
    }

    /**
     * Preload acls for entities
     *
     * @param Collection $entities
     */
    protected function loadAcls($entities)
    {
        $aclCollectionCache = $this->getAclCollectionCache();

        try {
            if ($aclCollectionCache) {
                $sortedEntities = [];
                foreach ($entities as $entity) {
                    $sortedEntities[get_class($entity)][] = $entity;
                }
                foreach ($sortedEntities as $entitiesGroup) {
                    if ($token = $this->tokenStorage->getToken()) {
                        $aclCollectionCache->cache($entitiesGroup, [$token]);
                    } else {
                        $aclCollectionCache->cache($entitiesGroup);
                    }
                }
            }
        } catch (NotAllAclsFoundException $e) {
            // At least we tried...
        } catch (AclNotFoundException $e) {
            // At least we tried...
        }
    }
}
