<?php

/**
 * Zym Framework
 *
 * This file is part of the Zym package.
 *
 * @link      https://github.com/geoffreytran/zym for the canonical source repository
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3 License
 */

namespace Zym\Bundle\UserBundle\Entity;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\UnitOfWork;
use FOS\UserBundle\Doctrine\UserManager as AbstractUserManager;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Util\CanonicalFieldsUpdater;
use FOS\UserBundle\Util\PasswordUpdaterInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\Security\Acl\Domain\AclCollectionCache;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Acl\Domain\UserSecurityIdentity;
use Symfony\Component\Security\Acl\Exception\AclAlreadyExistsException;
use Symfony\Component\Security\Acl\Exception\AclNotFoundException;
use Symfony\Component\Security\Acl\Exception\NotAllAclsFoundException;
use Symfony\Component\Security\Acl\Model\MutableAclProviderInterface;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Zym\Bundle\FrameworkBundle\Model\PageableRepositoryInterface;

/**
 * Class UserManager
 *
 * @package Zym\Bundle\UserBundle\Entity
 * @author  Geoffrey Tran <geoffrey.tran@gmail.com>
 */
class UserManager extends AbstractUserManager
{
    /**
     * @var MutableAclProviderInterface
     */
    private $aclProvider;

    /**
     * @var AclCollectionCache
     */
    private $aclCollectionCache;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * Constructor.
     *
     * @param PasswordUpdaterInterface    $passwordUpdater
     * @param CanonicalFieldsUpdater      $canonicalFieldsUpdater
     * @param ObjectManager               $om
     * @param string                      $class
     * @param PaginatorInterface          $paginator
     * @param MutableAclProviderInterface $aclProvider
     * @param AclCollectionCache          $aclCollectionCache
     */
    public function __construct(PasswordUpdaterInterface $passwordUpdater,
                                CanonicalFieldsUpdater $canonicalFieldsUpdater,
                                ObjectManager $om,
                                $class,
                                PaginatorInterface $paginator,
                                MutableAclProviderInterface $aclProvider,
                                AclCollectionCache $aclCollectionCache = null)
    {
        parent::__construct($passwordUpdater, $canonicalFieldsUpdater, $om, $class);

        if ($this->getRepository() instanceof PageableRepositoryInterface) {
            $this->getRepository()->setPaginator($paginator);
        }

        $this->setAclProvider($aclProvider);

        if ($aclCollectionCache) {
            $this->setAclCollectionCache($aclCollectionCache);
        }
    }

    /**
     * Create a user
     *
     * @param UserInterface $user
     * @return UserInterface
     */
    public function addUser(UserInterface $user)
    {
        $this->createEntity($user);

        return $user;
    }

    /**
     * Save a user
     *
     * @param UserInterface $user
     * @param bool          $andFlush
     */
    public function saveUser(UserInterface $user, $andFlush = true)
    {
        $this->updateCanonicalFields($user);
        $this->updatePassword($user);

        if ($this->objectManager->getUnitOfWork()->getEntityState($user) === UnitOfWork::STATE_NEW) {
            $this->createEntity($user, $andFlush);
        } else {
            $this->saveEntity($user, $andFlush);
        }
    }

    /**
     * Updates a user.
     *
     * @param UserInterface $user
     * @param bool          $andFlush Whether to flush the changes (default true)
     */
    public function updateUser(UserInterface $user, $andFlush = true)
    {
        $this->saveUser($user, $andFlush);
    }

    /**
     * Delete a user
     *
     * @param UserInterface $user
     */
    public function deleteUser(UserInterface $user)
    {
        $this->deleteEntity($user);
    }

    /**
     * Find users
     *
     * @param array $criteria
     * @param integer $page
     * @param integer $limit
     * @param array $orderBy
     * @return PaginatorInterface
     */
    public function findUsers(array $criteria = null, $page = 1, $limit = 50, array $orderBy = null)
    {
        $entities = $this->getRepository()->findUsers($criteria, $page, $limit, $orderBy);
        $this->loadAcls($entities);
        return $entities;
    }

    /**
     * Get the acl provider
     *
     * @return MutableAclProviderInterface
     */
    public function getAclProvider()
    {
        return $this->aclProvider;
    }

    /**
     * Set the acl provider
     *
     * @param MutableAclProviderInterface $aclProvider
     * @return UserManager
     */
    public function setAclProvider(MutableAclProviderInterface $aclProvider)
    {
        $this->aclProvider = $aclProvider;
        return $this;
    }

    /**
     * Get the acl collection cache
     *
     * @return AclCollectionCache
     */
    public function getAclCollectionCache()
    {
        return $this->aclCollectionCache;
    }

    /**
     * Set the acl collection cache
     *
     * @param AclCollectionCache $aclCollectionCache
     */
    public function setAclCollectionCache(AclCollectionCache $aclCollectionCache)
    {
        $this->aclCollectionCache = $aclCollectionCache;
    }

    /**
     * Get the token storage
     *
     * @return TokenStorageInterface
     */
    public function getTokenStorage()
    {
        return $this->tokenStorage;
    }

    /**
     * Set the token storage
     *
     * @param TokenStorageInterface $tokenStorage
     */
    public function setTokenStorage(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * Create an entity
     *
     * @param object $entity
     * @return object
     * @throws \Exception
     */
    protected function createEntity($entity)
    {
        // Persist
        $om = $this->objectManager;

        $om->beginTransaction();

        try {
            $om->persist($entity);
            $om->flush();

            // Acl
            $aclProvider = $this->aclProvider;
            $oid         = ObjectIdentity::fromDomainObject($entity);

            try {
                $acl = $aclProvider->createAcl($oid);

                // Users shouldn't be able to change there own roles
                $builder = new MaskBuilder();
                $builder->add('view')
                        ->add('edit');

                $mask = $builder->get();
                $acl->insertObjectAce(UserSecurityIdentity::fromAccount($entity), $mask);

                $builder = new MaskBuilder();
                $builder->add('delete');

                $mask = $builder->get();
                $acl->insertObjectAce(UserSecurityIdentity::fromAccount($entity), $mask, 0, false);

                $aclProvider->updateAcl($acl);
            } catch (AclAlreadyExistsException $e) {

            }

            $om->commit();
        } catch (\Exception $e) {
            $om->rollback();
            throw $e;
        }

        return $entity;
    }

    /**
     * Delete an entity
     *
     * @param object $entity
     * @return object
     * @throws \Exception
     */
    protected function deleteEntity($entity)
    {
        // Persist
        $om = $this->objectManager;

        $om->beginTransaction();

        try {
            // Acl
            $aclProvider = $this->aclProvider;
            $oid         = ObjectIdentity::fromDomainObject($entity);
            $acl         = $aclProvider->deleteAcl($oid);

            $om->remove($entity);
            $om->flush();

            $om->commit();
        } catch (\Exception $e) {
            $om->rollback();
            throw $e;
        }

        return $entity;
    }

    /**
     * Save an entity
     *
     * @param object $entity
     * @param bool $andFlush
     * @return object|null
     */
    protected function saveEntity($entity, $andFlush = true)
    {
        if ($this->objectManager->getUnitOfWork()->getEntityState($entity) == UnitOfWork::STATE_NEW) {
            return $this->createEntity($entity, $andFlush);
        } else {
            $em = $this->objectManager;

            if (method_exists($entity, 'setUpdatedAt')) {
                $entity->setUpdatedAt(new \DateTime());
            }

            $em->persist($entity);

            if ($andFlush) {
                $em->flush();
            }
        }
    }

    /**
     * Preload acls for entities
     *
     * @param array $entities
     */
    protected function loadAcls($entities)
    {
        $aclCollectionCache = $this->getAclCollectionCache();

        try {
            if ($aclCollectionCache) {
                $sortedEntities = [];
                foreach ($entities as $entity) {
                    $sortedEntities[get_class($entity)][] = $entity;
                }
                foreach ($sortedEntities as $entitiesGroup) {
                    $aclCollectionCache->cache($entitiesGroup);
                }
            }
        } catch (NotAllAclsFoundException $e) {
            // At least we tried...
        } catch (AclNotFoundException $e) {
            // At least we tried...
        }
    }
}
