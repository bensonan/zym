<?php

/**
 * Zym Framework
 *
 * This file is part of the Zym package.
 *
 * @link      https://github.com/geoffreytran/zym for the canonical source repository
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3 License
 */

namespace Zym\Bundle\UserBundle\Tests\Entity;

use Doctrine\Common\Persistence\Mapping\ClassMetadata;
use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManagerInterface;
use FOS\UserBundle\Util\CanonicalFieldsUpdater;
use FOS\UserBundle\Util\PasswordUpdaterInterface;
use Knp\Component\Pager\PaginatorInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Security\Acl\Model\MutableAclProviderInterface;
use Zym\Bundle\UserBundle\Entity\User;
use Zym\Bundle\UserBundle\Entity\UserManager;

/**
 * Zym User Bundle
 *
 * @author    Geoffrey Tran
 * @copyright Copyright (c) 2011 Zym. (http://www.zym.com/)
 */
class UserManagerTest extends TestCase
{
    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var ObjectRepository
     */
    private $repository;

    /**
     * @var PasswordUpdaterInterface
     */
    private $passwordUpdater;

    /**
     * @var CanonicalFieldsUpdater
     */
    private $canonicalFieldsUpdater;

    /**
     * @var PaginatorInterface
     */
    private $paginator;

    /**
     * @var MutableAclProviderInterface
     */
    private $aclProvider;

    /**
     * {@inheritdoc}
     */
    protected function setUp()
    {
        $this->passwordUpdater = $this->createMock(PasswordUpdaterInterface::class);
        $this->canonicalFieldsUpdater = $this->createMock(CanonicalFieldsUpdater::class);
        $this->entityManager = $this->getMockEntityManager();
        $this->paginator = $this->createMock(PaginatorInterface::class);
        $this->aclProvider = $this->createMock(MutableAclProviderInterface::class);
        $this->repository = $this->createMock(ObjectRepository::class);

        $this->entityManager->expects($this->any())
            ->method('getRepository')
            ->will($this->returnValue($this->repository));

        $this->userManager = new UserManager(
            $this->passwordUpdater,
            $this->canonicalFieldsUpdater,
            $this->entityManager,
            User::class,
            $this->paginator,
            $this->aclProvider
        );
    }

    public function testLoadUserByUsernameWithMissingUser()
    {
        $this->repository->expects($this->once())
            ->method('findOneBy')
            ->willReturn(null);

        $user = $this->userManager->findUserByUsernameOrEmail('jack');

        $this->assertNull($user);
    }

    public function testLoadUserByUsernameWithEmail()
    {
        $email = 'JACK@MSN.COM';
        $emailCanonical = 'jack@msn.com';

        $this->canonicalFieldsUpdater->expects($this->once())
            ->method('canonicalizeEmail')
            ->with($email)
            ->willReturn($emailCanonical);

        $this->repository->expects($this->once())
            ->method('findOneBy')
            ->with(['emailCanonical' => $emailCanonical]);

        $this->userManager->findUserByUsernameOrEmail($email);
    }

    public function testLoadUserByUsernameWithUsername()
    {
        $username = 'JACK';
        $usernameCanonical = 'jack';

        $this->canonicalFieldsUpdater->expects($this->once())
            ->method('canonicalizeUsername')
            ->with($username)
            ->willReturn($usernameCanonical);

        $this->repository->expects($this->once())
            ->method('findOneBy')
            ->with(['usernameCanonical' => $usernameCanonical]);

        $this->userManager->findUserByUsernameOrEmail($username);
    }

    private function getMockEntityManager()
    {
        $em = $this->createMock(EntityManagerInterface::class);

        $class = $this->createMock(ClassMetadata::class);
        $class->expects($this->any())
            ->method('getName')
            ->will($this->returnValue(User::class));

        $em->expects($this->any())
           ->method('getClassMetadata')
           ->with($this->equalTo(User::class))
           ->will($this->returnValue($class));

        return $em;
    }
}
