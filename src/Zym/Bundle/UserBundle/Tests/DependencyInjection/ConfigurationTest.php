<?php

/**
 * Zym Framework
 *
 * This file is part of the Zym package.
 *
 * @link      https://github.com/geoffreytran/zym for the canonical source repository
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3 License
 */

namespace Zym\Bundle\UserBundle\Tests\DependencyInjection;

use PHPUnit\Framework\TestCase;
use Symfony\Component\Config\Definition\Processor;
use Zym\Bundle\UserBundle\DependencyInjection\Configuration;

/**
 * Class ConfigurationTest
 *
 * @package Zym\Bundle\UserBundle\Tests\DependencyInjection
 * @author  Geoffrey Tran <geoffrey.tran@gmail.com>
 */
class ConfigurationTest extends TestCase
{
    /**
     * @var Configuration
     */
    private $configuration;

    /**
     * {@inheritdoc}
     */
    protected function setUp()
    {
        $this->configuration = new Configuration();
    }

    /**
     * {@inheritdoc}
     */
    protected function tearDown()
    {
        unset($this->configuration);
    }

    public function testGetConfigTreeBuilderHasRootNode()
    {
        $processor = new Processor();
        $configuration = $this->configuration;

        $config = $processor->processConfiguration($configuration, [
            'zym_user' => ['db_driver' => 'orm'],
        ]);

        $this->assertArrayHasKey('db_driver', $config);
    }
}
