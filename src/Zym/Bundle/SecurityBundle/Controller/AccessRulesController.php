<?php

/**
 * Zym Framework
 *
 * This file is part of the Zym package.
 *
 * @link      https://github.com/geoffreytran/zym for the canonical source repository
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3 License
 */

namespace Zym\Bundle\SecurityBundle\Controller;

use JMS\SecurityExtraBundle\Annotation\SecureParam;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Zym\Bundle\SecurityBundle\Entity\AccessRule;
use Zym\Bundle\SecurityBundle\Entity\AccessRuleManager;
use Zym\Bundle\SecurityBundle\Form\AccessRuleType;
use Zym\Bundle\SecurityBundle\Form\DeleteType;

/**
 * Access Controls Controller
 *
 * @author    Geoffrey Tran
 * @copyright Copyright (c) 2011 Zym. (http://www.zym.com/)
 */
class AccessRulesController extends Controller
{
    /**
     * @Route(
     *     "/", name="zym_security_access_rules")
     * @Template()
     *
     * @param Request $request
     */
    public function listAction(Request $request)
    {
        $page     = $request->query->get('page', 1);
        $limit    = $request->query->get('limit', 50);
        $orderBy  = $request->query->get('orderBy');
        $filterBy = $request->query->get('filterBy');

        $accessRuleManager = $this->get('zym_security.access_rule_manager');
        $accessRules       = $accessRuleManager->findAccessRules($filterBy, $page, $limit, $orderBy);

        return [
            'accessRules' => $accessRules,
        ];
    }

    /**
     * @Route("/add", name="zym_security_access_rules_add")
     * @Template()
     *
     * @param Request $request
     */
    public function addAction(Request $request)
    {
        $authChecker = $this->get('security.authorization_checker');

        if (!$authChecker->isGranted('OPERATOR', new ObjectIdentity('class', AccessRule::class))) {
            throw new AccessDeniedException();
        }

        $accessRule = new AccessRule();
        $form = $this->createForm(AccessRuleType::class, $accessRule);

        if ($request->isMethod(Request::METHOD_POST)) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $accessRuleManager = $this->get('zym_security.access_rule_manager');
                $accessRuleManager->createAccessRule($accessRule);

                $translator = $this->get('translator');

                $request->getSession()->setFlash($translator->trans('Created the new access rule successfully.'), 'success');

                return $this->redirectToRoute('zym_security_access_rules');
            }
        }

        return [
            'form' => $form->createView(),
        ];
    }

    /**
     * @Route("/{id}/edit", name="zym_security_access_rules_edit")
     * @Template()
     * @SecureParam(name="accessRule", permissions="EDIT")
     *
     * @param Request $request
     * @param AccessRule $accessRule
     */
    public function editAction(Request $request, AccessRule $accessRule)
    {
        $origAccessRule = clone $accessRule;
        $form = $this->createForm(AccessRuleType::class, $accessRule);

        if ($request->isMethod(Request::METHOD_POST)) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $accessRuleManager = $this->get('zym_security.access_rule_manager');
                $accessRuleManager->saveAccessRule($accessRule);

                $translator = $this->get('translator');

                $request->getSession()->getFlashBag()->add('success', $translator->trans('Changes saved!'));

                return $this->redirectToRoute('zym_security_access_rules');
            }
        }

        return [
            'accessRule' => $origAccessRule,
            'form'       => $form->createView(),
        ];
    }

    /**
     * Delete an access rule
     *
     * @Route(
     *     "/{id}",
     *     requirements={},
     *     methods={"DELETE"}
     * )
     *
     * @Route(
     *     "/{id}/delete.{_format}",
     *     name="zym_security_access_rules_delete",
     *     defaults = {
     *         "_format" = "html"
     *     },
     *     requirements = {
     *         "_format" = "html|json|ajax"
     *     }
     * )
     *
     * @Template()
     *
     * @SecureParam(name="accessRule", permissions="DELETE")
     *
     * @param Request $request
     * @param AccessRule $accessRule
     */
    public function deleteAction(Request $request, AccessRule $accessRule)
    {
        $origAccessRule = clone $accessRule;

        /** @var AccessRuleManager $accessRuleManager */
        $accessRuleManager = $this->get('zym_security.access_rule_manager');

        $form = $this->createForm(DeleteType::class, $accessRule);

        if ($request->isMethod(Request::METHOD_POST)) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $accessRuleManager->deleteAccessRule($accessRule);

                $translator = $this->get('translator');

                $request->getSession()->setFlash($translator->trans('Access rule deleted.'), 'success');

                return $this->redirectToRoute('zym_security_access_rules');
            }
        }

        if ($request->isMethod(Request::METHOD_DELETE)) {
            $accessRuleManager->deleteAccessRule($accessRule);

            return $this->redirectToRoute('zym_security_access_rules');
        }

        return [
            'accessRule' => $origAccessRule,
            'form'       => $form->createView(),
        ];
    }
}
