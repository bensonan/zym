<?php

/**
 * Zym Framework
 *
 * This file is part of the Zym package.
 *
 * @link      https://github.com/geoffreytran/zym for the canonical source repository
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3 License
 */

namespace Zym\Bundle\SecurityBundle\Controller;

use JMS\SecurityExtraBundle\Annotation\SecureParam;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Zym\Bundle\SecurityBundle\Entity\AclClass;
use Zym\Bundle\SecurityBundle\Entity\AclClassManager;
use Zym\Bundle\SecurityBundle\Form\AclEntryType;
use Zym\Bundle\SecurityBundle\Form\DeleteType;

/**
 * Acl Object Aces Controller
 *
 * @author    Geoffrey Tran
 * @copyright Copyright (c) 2011 Zym. (http://www.zym.com/)
 */
class AclObjectAcesController extends Controller
{
    /**
     * @Route(
     *     "{type}/{identifier}.{_format}",
     *     name="zym_security_acl_object_aces",
     *     defaults = { "_format" = "html" }
     * )
     * @Template()
     */
    public function listAction(Request $request, $identifier, $type)
    {
        $page = $request->query->get('page', 1);

        $aclProvider = $this->get('security.acl.provider');

        $oid = new ObjectIdentity($identifier, $type);
        $acl = $aclProvider->findAcl($oid);

        if (!$acl) {
            throw $this->createNotFoundException('Index does not exist');
        }

        return [
            'acl' => $acl,
            'oid' => $oid,
        ];
    }

    /**
     * @Route("/add/{classType}", name="zym_security_acl_entries_add")
     * @Template()
     */
    public function addAction(Request $request, AclClass $aclClass)
    {
        $authChecker = $this->get('security.authorization_checker');

        if (!$authChecker->isGranted('OPERATOR', new ObjectIdentity('class', $aclClass->getClassType()))) {
            throw new AccessDeniedException();
        }

        $aclProvider = $this->get('security.acl.provider');

        $oid = new ObjectIdentity('class', $aclClass->getClassType());
        $acl = $aclProvider->findAcl($oid);

        if (!$acl) {
            throw $this->createNotFoundException('Index does not exist');
        }

        $form = $this->createForm(AclEntryType::class, $aclClass);

        if ($request->isMethod(Request::METHOD_POST)) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $acl->insertClassAce($index, $classAce->getMask());

                $aclProvider->updateAcl($acl);
                return $this->redirectToRoute('zym_security_acl_entries');
            }
        }

        return [
            'form' => $form->createView(),
        ];
    }

    /**
     * @Route("/{classType}/edit/{index}", name="zym_security_acl_entries_edit")
     * @Template()
     *
     * @SecureParam(name="aclClass", permissions="EDIT")
     */
    public function editAction(Request $request, AclClass $aclClass, $index)
    {
        $origAclClass = clone $aclClass;

        $aclProvider = $this->get('security.acl.provider');

        $oid = new ObjectIdentity('class', $aclClass->getClassType());
        $acl = $aclProvider->findAcl($oid);

        $classAces = $acl->getClassAces();

        if (!isset($classAces[$index])) {
            throw $this->createNotFoundException('Index does not exist');
        }

        $classAce = clone $classAces[$index];

        $form = $this->createForm(AclEntryType::class, $classAce);

        if ($request->isMethod(Request::METHOD_POST)) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $acl->updateClassAce($index, $classAce->getMask());

                $aclProvider->updateAcl($acl);

                return $this->redirectToRoute('zym_security_acl_entries');
            }
        }

        return [
            'aclClass' => $origAclClass,
            'index'    => $index,
            'form'     => $form->createView(),
        ];
    }

    /**
     * Delete a aclClass
     *
     * @param AclClass $aclClass
     *
     * @Route(
     *     "{type}/{identifier}",
     *     requirements={},
     *     methods={"DELETE"}
     * )
     *
     * @Route(
     *     "{type}/{identifier}/delete.{_format}",
     *     name="zym_security_acl_object_aces_delete",
     *     defaults = {
     *         "_format" = "html"
     *     },
     *     requirements = {
     *         "_format" = "html|json|ajax"
     *     }
     * )
     *
     * @Template()
     *
     * SecureParam(name="aclClass", permissions="DELETE")
     */
    public function deleteAction(Request $request, $identifier, $type)
    {
        $origNode = clone $aclClass;

        /** @var AclClassManager $aclClassManager */
        $aclClassManager = $this->get('zym_security.acl_security_identity_manager');
        $form = $this->createForm(DeleteType::class, $aclClass);

        if ($request->isMethod(Request::METHOD_POST)) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $aclClassManager->deleteAclClass($aclClass);

                $translator = $this->get('translator');

                $request->getSession()->setFlash($translator->trans('Role Deleted'), 'success');

                return $this->redirectToRoute('zym_security_acl_entries');
            }
        }

        if ($request->isMethod(Request::METHOD_DELETE)) {
            $aclClassManager->deleteNode($aclClass);

            return $this->redirectToRoute('zym_security_acl_entries');
        }

        return [
            'aclClass' => $origNode,
            'form' => $form->createView(),
        ];
    }
}
