<?php

/**
 * Zym Framework
 *
 * This file is part of the Zym package.
 *
 * @link      https://github.com/geoffreytran/zym for the canonical source repository
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3 License
 */

namespace Zym\Bundle\SecurityBundle\Controller;

use JMS\SecurityExtraBundle\Annotation\SecureParam;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Acl\Domain\RoleSecurityIdentity;
use Symfony\Component\Security\Acl\Exception\AclNotFoundException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Zym\Bundle\SecurityBundle\Entity\AclClass;
use Zym\Bundle\SecurityBundle\Entity\AclEntry;
use Zym\Bundle\SecurityBundle\Form\AclEntryType;
use Zym\Bundle\SecurityBundle\Form\DeleteType;

/**
 * Acl Entries Controller
 *
 * @author    Geoffrey Tran
 * @copyright Copyright (c) 2011 Zym. (http://www.zym.com/)
 */
class AclEntriesController extends Controller
{
    /**
     * @Route(
     *     ".{_format}",
     *     name="zym_security_acl_entries",
     *     defaults = { "_format" = "html" }
     * )
     * @Template()
     *
     * @param Request $request
     */
    public function listAction(Request $request)
    {
        $page = $request->query->get('page', 1);

        $aclClassManager = $this->get('zym_security.acl_class_manager');
        $aclClasses      = $aclClassManager->findAclClasses([], $page);

        $aclProvider = $this->get('security.acl.provider');

        $oids = [];
        foreach ($aclClasses as $aclClass) {
            $oid = new ObjectIdentity('class', $aclClass->getClassType());

            try {
                $aclProvider->findAcl($oid);
            } catch (AclNotFoundException $e) {
                // Missing class level entry, add it
                $acl = $aclProvider->createAcl($oid);
                $aclProvider->updateAcl($acl);
                continue;
            }

            $oids[] = $oid;
        }

        $acls = $aclProvider->findAcls($oids);

        return [
            'aclClasses' => $aclClasses,
            'acls'       => $acls,
        ];
    }

    /**
     * @Route("/add/{classType}", name="zym_security_acl_entries_add")
     * @Template()
     *
     * @param Request  $request
     * @param AclClass $aclClass
     */
    public function addAction(Request $request, AclClass $aclClass)
    {
        $authChecker = $this->get('security.authorization_checker');

        if (!$authChecker->isGranted('OPERATOR', new ObjectIdentity('class', $aclClass->getClassType()))) {
            throw new AccessDeniedException();
        }

        $aclProvider = $this->get('security.acl.provider');

        $oid = new ObjectIdentity('class', $aclClass->getClassType());
        $acl = $aclProvider->findAcl($oid);

        if (!$acl) {
            throw $this->createNotFoundException('Index does not exist');
        }

        $classAce = new AclEntry();
        $form = $this->createForm(AclEntryType::class, $classAce);

        if ($request->isMethod(Request::METHOD_POST)) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $acl->insertClassAce(new RoleSecurityIdentity($classAce->getSecurityIdentity()),
                                    $classAce->getMask(), 0, $classAce->getMask(), $classAce->getStrategy());

                $aclProvider->updateAcl($acl);
                return $this->redirectToRoute('zym_security_acl_entries');
            }
        }

        return [
            'aclClass' => $aclClass,
            'form'     => $form->createView(),
        ];
    }

    /**
     * @Route("/{classType}/{index}/edit", name="zym_security_acl_entries_edit")
     * @Template()
     *
     * @SecureParam(name="aclClass", permissions="OPERATOR")
     */
    public function editAction(Request $request, AclClass $aclClass, $index)
    {
        $authChecker = $this->get('security.authorization_checker');

        if (!$authChecker->isGranted('OPERATOR', new ObjectIdentity('class', $aclClass->getClassType()))) {
            throw new AccessDeniedException();
        }

        $origAclClass = clone $aclClass;

        $aclProvider = $this->get('security.acl.provider');

        $oid         = new ObjectIdentity('class', $aclClass->getClassType());
        $acl         = $aclProvider->findAcl($oid);

        $classAces = $acl->getClassAces();
        if (!isset($classAces[$index])) {
            throw $this->createNotFoundException('Index does not exist');
        }

        $classAce = clone $classAces[$index];

        $form    = $this->createForm(AclEntryType::class, $classAce);

        if ($request->isMethod(Request::METHOD_POST)) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $acl->updateClassAce($index, $classAce->getMask());

                $aclProvider->updateAcl($acl);

                return $this->redirectToRoute('zym_security_acl_entries');
            }
        }

        return [
            'aclClass' => $origAclClass,
            'index'    => $index,
            'form'     => $form->createView(),
        ];
    }

    /**
     * Delete a aclClass
     *
     * @param AclClass $aclClass
     *
     * @Route(
     *     "/{classType}/{index}",
     *     requirements={},
     *     methods={"DELETE"}
     * )
     *
     * @Route(
     *     "/{classType}/{index}/delete.{_format}",
     *     name="zym_security_acl_entries_delete",
     *     defaults = {
     *         "_format" = "html"
     *     },
     *     requirements = {
     *         "_format" = "html|json|ajax"
     *     }
     * )
     *
     * @Template()
     *
     * @SecureParam(name="aclClass", permissions="DELETE")
     */
    public function deleteAction(Request $request, AclClass $aclClass, $index)
    {
        $origAclClass = clone $aclClass;

        $aclProvider = $this->get('security.acl.provider');

        $oid = new ObjectIdentity('class', $aclClass->getClassType());
        /** @var \Symfony\Component\Security\Acl\Domain\Acl $acl */
        $acl = $aclProvider->findAcl($oid);

        $classAces = $acl->getClassAces();

        if (!isset($classAces[$index])) {
            throw $this->createNotFoundException('Index does not exist');
        }

        $classAce = clone $classAces[$index];

        $form = $this->createForm(DeleteType::class, $classAce);

        if ($request->isMethod(Request::METHOD_POST)) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $acl->deleteClassAce($index);
                $aclProvider->updateAcl($acl);

                $translator = $this->get('translator');

                $request->getSession()->setFlash($translator->trans('Acl Entry Deleted'), 'success');

                $referer = $request->headers->get('referer');
                return $this->redirect($referer);
            }
        }

        if ($request->isMethod(Request::METHOD_DELETE)) {
            $acl->deleteClassAce($index);
            $aclProvider->updateAcl($acl);

            return $this->redirectToRoute('zym_security_acl_entries');
        }

        return [
            'aclClass' => $origAclClass,
            'index'    => $index,
            'form'     => $form->createView(),
        ];
    }
}
