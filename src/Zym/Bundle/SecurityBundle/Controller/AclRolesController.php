<?php

/**
 * Zym Framework
 *
 * This file is part of the Zym package.
 *
 * @link      https://github.com/geoffreytran/zym for the canonical source repository
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3 License
 */

namespace Zym\Bundle\SecurityBundle\Controller;

use JMS\SecurityExtraBundle\Annotation\SecureParam;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Zym\Bundle\SecurityBundle\Entity\AclSecurityIdentity;
use Zym\Bundle\SecurityBundle\Entity\AclSecurityIdentityManager;
use Zym\Bundle\SecurityBundle\Form\AclSecurityIdentityType;
use Zym\Bundle\SecurityBundle\Form\DeleteType;
use Zym\Bundle\SecurityBundle\Form\NodeType;

/**
 * Acl Roles Controller
 *
 * @author    Geoffrey Tran
 * @copyright Copyright (c) 2011 Zym. (http://www.zym.com/)
 */
class AclRolesController extends Controller
{
    /**
     * @Route(
     *     ".{_format}",
     *     name="zym_security_acl_roles",
     *     defaults={
     *         "_format" = "html"
     *     }
     * )
     * @Template()
     */
    public function listAction(Request $request)
    {
        $page     = $request->query->get('page', 1);
        $limit    = $request->query->get('limit', 50);
        $orderBy  = $request->query->get('orderBy');
        $filterBy = array_merge(['username' => 0], (array)$request->query->get('filterBy', []));

        $roleManager = $this->get('zym_security.acl_security_identity_manager');
        $roles       = $roleManager->findAclSecurityIdentities($filterBy, $page, $limit, $orderBy);

        return [
            'roles' => $roles,
        ];
    }

    /**
     * @Route("/add", name="zym_security_acl_roles_add")
     * @Template()
     */
    public function addAction(Request $request)
    {
        $authChecker = $this->get('security.authorization_checker');

        if (!$authChecker->isGranted('CREATE', new ObjectIdentity('class', AclSecurityIdentity::class))) {
            throw new AccessDeniedException();
        }

        $role = new AclSecurityIdentity();
        $form = $this->createForm(AclSecurityIdentityType::class, $role);

        if ($request->isMethod(Request::METHOD_POST)) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $roleManager = $this->get('zym_security.acl_security_identity_manager');
                $roleManager->createAclSecurityIdentity($role);

                $translator = $this->get('translator');

                $request->getSession()->setFlash($translator->trans('Created the new role successfully.'), 'success');

                return $this->redirectToRoute('zym_security_acl_roles');
            }
        }

        return [
            'form' => $form->createView(),
        ];
    }

    /**
     * @Route("/{id}/edit", name="zym_security_acl_roles_edit")
     * @Template()
     *
     * @SecureParam(name="role", permissions="EDIT")
     */
    public function editAction(Request $request, AclSecurityIdentity $role)
    {
        $origNode = clone $role;

        $form = $this->createForm(NodeType::class, $role);

        if ($request->isMethod(Request::METHOD_POST)) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $roleManager = $this->get('zym_role.role_manager');
                $roleManager->saveNode($role);

                $translator = $this->get('translator');

                $request->getSession()->getFlashBag()->add('success', $translator->trans('Changes saved!'));

                return $this->redirectToRoute('backend_roles');
            }
        }

        return [
            'role' => $origNode,
            'form' => $form->createView(),
        ];
    }

    /**
     * Delete a role
     *
     * @param AclSecurityIdentity $role
     *
     * @Route(
     *     "/{id}",
     *     requirements={},
     *     methods={"DELETE"}
     * )
     *
     * @Route(
     *     "/{id}/delete.{_format}",
     *     name="zym_security_acl_roles_delete",
     *     defaults = {
     *         "_format" = "html"
     *     },
     *     requirements = {
     *         "_format" = "html|json|ajax"
     *     }
     * )
     *
     * @Template()
     *
     * @SecureParam(name="role", permissions="DELETE")
     */
    public function deleteAction(Request $request, AclSecurityIdentity $role)
    {
        $origNode = clone $role;

        /** @var AclSecurityIdentityManager $roleManager */
        $roleManager = $this->get('zym_security.acl_security_identity_manager');
        $form = $this->createForm(DeleteType::class, $role);

        if ($request->isMethod(Request::METHOD_POST)) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $roleManager->deleteAclSecurityIdentity($role);

                $translator = $this->get('translator');

                $request->getSession()->setFlash($translator->trans('Role Deleted'), 'success');

                return $this->redirectToRoute('zym_security_acl_roles');
            }
        }

        if ($request->isMethod(Request::METHOD_DELETE)) {
            $roleManager->deleteAclSecurityIdentity($role);

            return $this->redirectToRoute('zym_security_acl_roles');
        }

        return [
            'role' => $origNode,
            'form' => $form->createView(),
        ];
    }
}
