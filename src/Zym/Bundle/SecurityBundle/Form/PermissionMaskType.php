<?php

/**
 * Zym Framework
 *
 * This file is part of the Zym package.
 *
 * @link      https://github.com/geoffreytran/zym for the canonical source repository
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3 License
 */

namespace Zym\Bundle\SecurityBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;

class PermissionMaskType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addViewTransformer(new PermissionMaskTransformer());

        $builder
            ->add('view', CheckboxType::class, [
                'label'      => 'View',
                'help_block' => 'Whether someone is allowed to view the domain object.',
                'required'   => false,
            ])
            ->add('create', CheckboxType::class, [
                'label'      => 'Create',
                'help_block' => 'Whether someone is allowed to create the domain object.',
                'required'   => false,
            ])
            ->add('edit', CheckboxType::class, [
                'label'      => 'Edit',
                'help_block' => 'Whether someone is allowed to make changes to the domain object.',
                'required'   => false,
            ])
            ->add('delete', CheckboxType::class, [
                'label'      => 'Delete',
                'help_block' => 'Whether someone is allowed to delete the domain object.',
                'required'   => false,
            ])
            ->add('undelete', CheckboxType::class, [
                'label'      => 'Undelete',
                'help_block' => 'Whether someone is allowed to restore a previously deleted domain object.',
                'required'   => false,
            ])
            ->add('operator', CheckboxType::class, [
                'label'      => 'Operator',
                'help_block' => 'Whether someone is allowed to perform all of the above actions.',
                'required'   => false,
            ])
            ->add('master', CheckboxType::class, [
                'label'      => 'Master',
                'help_block' => 'Whether someone is allowed to perform all of the above actions, and in addition is allowed to grant any of the above permissions to others.',
                'required'   => false,
            ])
            ->add('iddqd', CheckboxType::class, [
                'label'      => 'IDDQD',
                'help_block' => 'Whether someone owns the domain object. An owner can perform any of the above actions and grant master and owner permissions.',
                'required'   => false,
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return '';
    }
}
