<?php

/**
 * Zym Framework
 *
 * This file is part of the Zym package.
 *
 * @link      https://github.com/geoffreytran/zym for the canonical source repository
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3 License
 */

namespace Zym\Bundle\SecurityBundle\Twig\Extension;

use Symfony\Component\Security\Acl\Permission\MaskBuilder;

class SecurityExtension extends \Twig_Extension
{
    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('acl_mask_to_array', [$this, 'aclMaskToArray'], ['is_safe' => ['html']]),
        ];
    }

    /**
     * Returns an array of mask strings from the single mask value
     *
     * @param integer $value
     * @return array
     */
    public function aclMaskToArray($value)
    {
        $values = [];

        $reflection = new \ReflectionClass(MaskBuilder::class);

        foreach ($reflection->getConstants() as $name => $cMask) {
            $cName = substr($name, 5);

            if (0 === strpos($name, 'MASK_')) {
                $maskValue = constant(sprintf('%s::%s', MaskBuilder::class, $name));

                if (($value & $maskValue) == $maskValue) {
                    $values[strtolower($cName)] = ucwords(strtolower($cName));
                }
            }
        }

        return $values;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'zym_security';
    }
}
