<?php

/**
 * Zym Framework
 *
 * This file is part of the Zym package.
 *
 * @link      https://github.com/geoffreytran/zym for the canonical source repository
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3 License
 */

namespace Zym\Bundle\FrameworkBundle\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\UnitOfWork;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\Security\Acl\Domain\AclCollectionCache;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Acl\Exception\AclAlreadyExistsException;
use Symfony\Component\Security\Acl\Exception\AclNotFoundException;
use Symfony\Component\Security\Acl\Exception\NotAllAclsFoundException;
use Symfony\Component\Security\Acl\Model\MutableAclProviderInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Zym\Bundle\FrameworkBundle\Model\PageableRepositoryInterface;

/**
 * Abstract Entity Manager
 *
 * @author    Geoffrey Tran
 * @copyright Copyright (c) 2011 Zym. (http://www.zym.com/)
 */
abstract class AbstractEntityManager
{
    /**
     * ObjectManager
     *
     * @var ObjectManager
     */
    protected $objectManager;

    /**
     * Repository
     *
     * @var AbstractEntityRepository
     */
    protected $repository;

    /**
     * Class
     *
     * @var string
     */
    protected $class;

    /**
     * Acl Provider
     *
     * @var MutableAclProviderInterface
     */
    protected $aclProvider;

    /**
     * Acl Collection Cache
     *
     * @var AclCollectionCache
     */
    protected $aclCollectionCache;

    /**
     * Security Token Storage
     *
     * @var TokenStorageInterface
     */
    protected $tokenStorage;

    /**
     * Construct
     *
     * @param ObjectManager               $objectManager
     * @param string                      $class
     * @param PaginatorInterface          $paginator
     * @param MutableAclProviderInterface $aclProvider
     * @param TokenStorageInterface       $tokenStorage
     * @param AclCollectionCache          $aclCollectionCache
     */
    public function __construct(ObjectManager $objectManager,
                                $class,
                                PaginatorInterface $paginator,
                                MutableAclProviderInterface $aclProvider,
                                TokenStorageInterface $tokenStorage = null,
                                AclCollectionCache $aclCollectionCache = null)
    {
        $this->setObjectManager($objectManager);
        $this->setRepository($objectManager->getRepository($class));

        $metadata = $objectManager->getClassMetadata($class);
        $this->class = $metadata->getName();

        if ($this->getRepository() instanceof PageableRepositoryInterface) {
            $this->getRepository()->setPaginator($paginator);
        }

        $this->setAclProvider($aclProvider);

        if ($tokenStorage) {
            $this->tokenStorage = $tokenStorage;
        }

        if ($aclCollectionCache) {
            $this->setAclCollectionCache($aclCollectionCache);
        }
    }


    /**
     * Find a node by criteria
     *
     * @param array $criteria
     * @return object
     */
    public function findOneBy(array $criteria)
    {
        return $this->repository->findOneBy($criteria);
    }

    /**
     * Get the object manager
     *
     * @return ObjectManager
     */
    public function getObjectManager()
    {
        return $this->objectManager;
    }

    /**
     * Set the object manager
     *
     * @param ObjectManager $objectManager
     * @return AbstractEntityManager
     */
    protected function setObjectManager(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;

        return $this;
    }

    /**
     * Get the entity manager
     *
     * @return EntityManagerInterface
     */
    public function getEntityManager()
    {
        return $this->objectManager;
    }

    /**
     * Set the entity manager
     *
     * @param EntityManagerInterface $entityManager
     * @return AbstractEntityManager
     */
    protected function setEntityManager(EntityManagerInterface $entityManager)
    {
        return $this->setObjectManager($entityManager);
    }

    /**
     * Get the entity repository
     *
     * @return EntityRepository
     */
    public function getRepository()
    {
        return $this->repository;
    }

    /**
     * Set the repository
     *
     * @param EntityRepository $repository
     * @return AbstractEntityManager
     */
    protected function setRepository(EntityRepository $repository)
    {
        $this->repository = $repository;
        return $this;
    }

    /**
     * Get the entity class
     *
     * @return string
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * Get the acl provider
     *
     * @return MutableAclProviderInterface
     */
    public function getAclProvider()
    {
        return $this->aclProvider;
    }

    /**
     * Set the acl provider
     *
     * @param MutableAclProviderInterface $aclProvider
     * @return AbstractEntityManager
     */
    public function setAclProvider(MutableAclProviderInterface $aclProvider)
    {
        $this->aclProvider = $aclProvider;
        return $this;
    }

    /**
     * Get the acl collection cache
     *
     * @return AclCollectionCache
     */
    public function getAclCollectionCache()
    {
        return $this->aclCollectionCache;
    }

    /**
     * Set the acl collection cache
     *
     * @param AclCollectionCache $aclCollectionCache
     * @return AbstractEntityManager
     */
    public function setAclCollectionCache(AclCollectionCache $aclCollectionCache)
    {
        $this->aclCollectionCache = $aclCollectionCache;
        return $this;
    }

    /**
     * Get the security token storage
     *
     * @return TokenStorageInterface
     */
    public function getSecurityTokenStorage()
    {
        return $this->tokenStorage;
    }

    /**
     * Set the security token storage
     *
     * @param TokenStorageInterface $tokenStorage
     * @return AbstractEntityManager
     */
    public function setSecurityTokenStorage(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
        return $this;
    }

    /**
     * Create an entity
     *
     * @param object $entity
     * @return object
     * @throws \Exception
     */
    protected function createEntity($entity)
    {
        $this->objectManager->beginTransaction();

        try {
            $this->objectManager->persist($entity);
            $this->objectManager->flush();

            // Acl
            $aclProvider = $this->aclProvider;
            $oid = ObjectIdentity::fromDomainObject($entity);

            try {
                $acl = $aclProvider->createAcl($oid);
                $aclProvider->updateAcl($acl);
            } catch (AclAlreadyExistsException $e) {
                // No need to do anything
            }

            $this->objectManager->commit();
        } catch (\Exception $e) {
            $this->objectManager->rollback();
            throw $e;
        }

        return $entity;
    }

    /**
     * Delete an entity
     *
     * @param object $entity
     * @return object
     * @throws \Exception
     */
    protected function deleteEntity($entity)
    {
        $this->objectManager->beginTransaction();

        try {
            // Acl
            $aclProvider = $this->aclProvider;
            $oid = ObjectIdentity::fromDomainObject($entity);
            $acl = $aclProvider->deleteAcl($oid);

            $this->objectManager->remove($entity);
            $this->objectManager->flush();

            $this->objectManager->commit();
        } catch (\Exception $e) {
            $this->objectManager->rollback();
            throw $e;
        }

        return $entity;
    }

    /**
     * Save an entity
     *
     * @param object $entity
     * @param boolean $andFlush
     * @return object|null
     */
    protected function saveEntity($entity, $andFlush = true)
    {
        if ($this->objectManager->getUnitOfWork()->getEntityState($entity) === UnitOfWork::STATE_NEW) {
            return $this->createEntity($entity, $andFlush);
        } else {
            $this->objectManager->persist($entity);
            if ($andFlush) {
                $this->objectManager->flush();
            }
        }

        return $entity;
    }

    /**
     * Preload acls for entities
     *
     * @param Collection $entities
     */
    protected function loadAcls($entities)
    {
        $aclCollectionCache = $this->getAclCollectionCache();

        try {
            if ($aclCollectionCache) {
                $sortedEntities = [];
                foreach ($entities as $entity) {
                    $sortedEntities[get_class($entity)][] = $entity;
                }
                foreach ($sortedEntities as $entitiesGroup) {
                    if ($token = $this->tokenStorage->getToken()) {
                        $aclCollectionCache->cache($entitiesGroup, [$token]);
                    } else {
                        $aclCollectionCache->cache($entitiesGroup);
                    }
                }
            }
        } catch (NotAllAclsFoundException $e) {
            // At least we tried...
        } catch (AclNotFoundException $e) {
            // At least we tried...
        }
    }
}
