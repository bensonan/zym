<?php

/**
 * Zym Framework
 *
 * This file is part of the Zym package.
 *
 * @link      https://github.com/geoffreytran/zym for the canonical source repository
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3 License
  */

namespace Zym\Bundle\RuntimeConfigBundle\Controller;

use JMS\SecurityExtraBundle\Annotation\Secure;
use JMS\SecurityExtraBundle\Annotation\SecureParam;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Zym\Bundle\RuntimeConfigBundle\Entity\Parameter;
use Zym\Bundle\RuntimeConfigBundle\Entity\ParameterManager;
use Zym\Bundle\RuntimeConfigBundle\Form\DeleteType;
use Zym\Bundle\RuntimeConfigBundle\Form\ParameterType;

/**
 * Runtime Config Controller
 *
 * @author    Geoffrey Tran
 * @copyright Copyright (c) 2011 Zym. (http://www.zym.com/)
 */
class ParametersController extends Controller
{
    /**
     * @Route("/", name="zym_runtime_config_parameters")
     * @Template()
     *
     * @param Request $request
     * @return array
     */
    public function listAction(Request $request)
    {
        $page     = $request->query->get('page', 1);
        $limit    = $request->query->get('limit', 50);
        $orderBy  = $request->query->get('orderBy');
        $filterBy = $request->query->get('filterBy');

        $parameterManager = $this->get('zym_runtime_config.parameter_manager');
        $parameters = $parameterManager->findParameters($filterBy, $page, $limit, $orderBy);

        return [
            'parameters' => $parameters,
        ];
    }

    /**
     * Show a parameter
     *
     * @Route(
     *     "/{name}/view.{_format}",
     *     name="zym_runtime_config_parameters_show",
     *     defaults = {
     *         "_format" = "html"
     *     },
     *     requirements = {
     *         "id" = "\d+",
     *         "_format" = "html|json"
     *     }
     * )
     * @ParamConverter("parameter", class="ZymRuntimeConfigBundle:Parameter")
     * @Template()
     *
     * @SecureParam(name="parameter", permissions="VIEW")
     *
     * @param Parameter $parameter
     * @return array
     */
    public function showAction(Parameter $parameter)
    {
        return [
            'parameter' => $parameter,
        ];
    }

    /**
     * @Route(
     *     "/add.{_format}",
     *     name="zym_runtime_config_parameters_add",
     *     defaults={
     *         "_format" = "html"
     *     }
     * )
     * @Template()
     *
     * @param Request $request
     * @return array
     */
    public function addAction(Request $request)
    {
        $authChecker = $this->get('security.authorization_checker');

        if (!$authChecker->isGranted('CREATE', new ObjectIdentity('class', Parameter::class))) {
            throw new AccessDeniedException();
        }

        $parameter = new Parameter();

        $form = $this->createForm(ParameterType::class, $parameter);

        if ($request->isMethod(Request::METHOD_POST)) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $parameterManager = $this->get('zym_runtime_config.parameter_manager');
                $parameterManager->createParameter($parameter);

                $translator = $this->get('translator');

                $request->getSession()->getFlashBag()->add('success', $translator->trans('Added successfully!'));

                return $this->redirectToRoute('zym_runtime_config_parameters');
            }
        }

        return [
            'form' => $form->createView(),
        ];
    }

    /**
     * @Route("/{name}/edit", name="zym_runtime_config_parameters_edit")
     * @ParamConverter("parameter", class="ZymRuntimeConfigBundle:Parameter")
     * @Template()
     *
     * @SecureParam(name="parameter", permissions="EDIT")
     *
     * @param Request $request
     * @param Parameter $parameter
     * @return array
     */
    public function editAction(Request $request, Parameter $parameter)
    {
        $origParameter = clone $parameter;
        $form = $this->createForm(ParameterType::class, $parameter);

        if ($request->isMethod(Request::METHOD_POST)) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $parameterManager = $this->get('zym_runtime_config.parameter_manager');
                $parameterManager->saveParameter($parameter);

                $translator = $this->get('translator');

                $request->getSession()->getFlashBag()->add('success', $translator->trans('Changes saved!'));

                return $this->redirectToRoute('zym_runtime_config_parameters');
            }
        }

        return [
            'parameter' => $origParameter,
            'form' => $form->createView(),
        ];
    }

    /**
     * Delete a parameter
     *
     * @param Parameter $parameter
     *
     * @Route(
     *     "/{name}",
     *     requirements={},
     *     methods={"DELETE"}
     * )
     * @Route(
     *     "/{name}/delete.{_format}",
     *     name="zym_runtime_config_parameters_delete",
     *     defaults = {
     *         "_format" = "html"
     *     },
     *     requirements = {
     *         "_format" = "html|json|ajax"
     *     }
     * )
     *
     * @Template()
     *
     * @SecureParam(name="parameter", permissions="DELETE")
     *
     * @param Request $request
     * @param Parameter $parameter
     */
    public function deleteAction(Request $request, Parameter $parameter)
    {
        $origParameter = clone $parameter;

        /** @var ParameterManager $parameterManager */
        $parameterManager = $this->get('zym_runtime_config.parameter_manager');
        $form = $this->createForm(DeleteType::class, $parameter);

        if ($request->isMethod(Request::METHOD_POST)) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $parameterManager->deleteParameter($parameter);

                $translator = $this->get('translator');

                $request->getSession()->setFlash($translator->trans('Parameter Deleted'), 'success');

                return $this->redirectToRoute('zym_runtime_config_parameters');
            }
        }

        if ($request->isMethod(Request::METHOD_DELETE)) {
            $parameterManager->deleteParameter($parameter);

            return $this->redirectToRoute('zym_runtime_config_parameters');
        }

        return [
            'parameter' => $origParameter,
            'form' => $form->createView(),
        ];
    }
}
