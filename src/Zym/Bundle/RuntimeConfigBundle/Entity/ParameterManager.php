<?php

/**
 * Zym Framework
 *
 * This file is part of the Zym package.
 *
 * @link      https://github.com/geoffreytran/zym for the canonical source repository
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3 License
 */

namespace Zym\Bundle\RuntimeConfigBundle\Entity;

use Doctrine\Common\Persistence\ObjectManager;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\Security\Acl\Model\MutableAclProviderInterface;
use Zym\Bundle\FrameworkBundle\Entity\AbstractEntityManager;

/**
 * Parameter Manager
 *
 * @package Zym\Bundle\UserBundle
 * @author  Geoffrey Tran <geoffrey.tran@gmail.com>
 */
class ParameterManager extends AbstractEntityManager
{
    /**
     * @param ObjectManager               $objectManager
     * @param string                      $class
     * @param PaginatorInterface          $paginator
     * @param MutableAclProviderInterface $aclProvider
     */
    public function __construct(ObjectManager $objectManager,
                                $class,
                                PaginatorInterface $paginator,
                                MutableAclProviderInterface $aclProvider)
    {
        parent::__construct($objectManager, $class, $paginator, $aclProvider);
    }

    /*
     * Create a parameter
     *
     * @param Parameter $parameter
     * @return Parameter
     */
    public function createParameter(Parameter $parameter)
    {
        parent::createEntity($parameter);

        return $parameter;
    }

    /**
     * Save a parameter
     *
     * @param Parameter $parameter
     * @param boolean $andFlush
     */
    public function saveParameter(Parameter $parameter, $andFlush = true)
    {
        parent::saveEntity($parameter, $andFlush);
    }

    /**
     * Delete a parameter
     *
     * @param Parameter $parameter
     */
    public function deleteParameter(Parameter $parameter)
    {
        parent::deleteEntity($parameter);
    }

    /**
     * Find parameters
     *
     * @param array $criteria
     * @param integer $page
     * @param integer $limit
     * @param array $orderBy
     * @return PaginationInterface
     */
    public function findParameters(array $criteria = null, $page = 1, $limit = 50, array $orderBy = null)
    {
        return $this->repository->findParameters($criteria, $page, $limit, $orderBy);
    }

    /**
     * Find a parameter by criteria
     *
     * @param array $criteria
     * @return Parameter
     */
    public function findParameterBy(array $criteria)
    {
        return $this->repository->findOneBy($criteria);
    }

    /**
     * Find parameter
     *
     * @param string $name
     * @return Parameter
     */
    public function findParameter($name)
    {
        return $this->repository->findOneBy(['name' => $name]);
    }
}
