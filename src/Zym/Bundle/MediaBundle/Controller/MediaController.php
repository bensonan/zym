<?php

/**
 * Zym Framework
 *
 * This file is part of the Zym package.
 *
 * @link      https://github.com/geoffreytran/zym for the canonical source repository
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3 License
 */

namespace Zym\Bundle\MediaBundle\Controller;

use JMS\SecurityExtraBundle\Annotation\Secure;
use JMS\SecurityExtraBundle\Annotation\SecureParam;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Zym\Bundle\MediaBundle\Entity\Media;
use Zym\Bundle\MediaBundle\Entity\MediaManager;
use Zym\Bundle\MediaBundle\Form\DeleteType;
use Zym\Bundle\MediaBundle\MediaPool;

/**
 * Class MediaController
 *
 * @package Zym\Bundle\MediaBundle\Controller
 * @author  Geoffrey Tran <geoffrey.tran@gmail.com>
 */
class MediaController extends Controller
{
    /**
     * @Route(
     *     ".{_format}",
     *     name="zym_media",
     *     defaults = { "_format" = "html" }
     * )
     * @Template()
     *
     * @param Request $request
     */
    public function listAction(Request $request)
    {
        $page     = $request->query->get('page', 1);
        $limit    = $request->query->get('limit', 50);
        $orderBy  = $request->query->get('orderBy');
        $filterBy = $request->query->get('filterBy');

        /** @var MediaManager $mediaManager */
        $mediaManager  = $this->get('zym_media.media_manager');
        $medias        = $mediaManager->findMedias($filterBy, $page, $limit, $orderBy);

        return [
            'medias' => $medias,
        ];
    }

    /**
     * @Route("/add", name="zym_media_add")
     * @Template()
     *
     * @param Request $request
     */
    public function addAction(Request $request)
    {
        $authChecker = $this->get('security.authorization_checker');

        if (!$authChecker->isGranted('CREATE', new ObjectIdentity('class', Media::class))) {
            throw new AccessDeniedException();
        }

        $media = new Media();
        $form = $this->createForm($this->get('zym_media.form.type.media'), $media, array(
            'provider' => 'zym_media.provider.image',
            'context'  => 'default'
        ));

        if ($request->isMethod(Request::METHOD_POST)) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                /** @var MediaManager $mediaManager */
                $mediaManager = $this->get('zym_media.media_manager');
                $mediaManager->createMedia($media);

                return $this->redirectToRoute('zym_media');
            }
        }

        return [
            'form' => $form->createView(),
        ];
    }

    /**
     * Edit a media
     *
     * @Route("/{id}/edit", name="zym_media_edit")
     * @ParamConverter("media", class="ZymMediaBundle:Media")
     * @Template()
     *
     * @SecureParam(name="media", permissions="EDIT")
     *
     * @param Request $request
     * @param Media   $media
     */
    public function editAction(Request $request, Media $media)
    {
        $origMedia = clone $media;
        $form = $this->createForm($this->get('zym_media.form.type.media'), $media, [
            'provider' => $media->getProviderName(),
            'context'  => $media->getContext(),
        ]);

        if ($request->isMethod(Request::METHOD_POST)) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                /** @var MediaManager $mediaManager */
                $mediaManager = $this->get('zym_media.media_manager');
                $mediaManager->saveMedia($media);

                return $this->redirectToRoute('zym_media');
            }
        }

        return array(
            'media' => $origMedia,
            'form' => $form->createView()
        );
    }

    /**
     * Delete a media
     *
     * @param Media $media
     *
     * @Route(
     *     "/{id}",
     *     requirements={},
     *     methods={"DELETE"}
     * )
     * @Route(
     *     "/{id}/delete.{_format}",
     *     name="zym_media_delete",
     *     defaults={
     *         "_format" = "html"
     *     }
     * )
     *
     * @Template()
     *
     * @SecureParam(name="media", permissions="DELETE")
     */
    public function deleteAction(Request $request, Media $media)
    {
        $origMedia = clone $media;

        /** @var MediaManager $mediaManager */
        $mediaManager = $this->get('zym_media.media_manager');
        $form        = $this->createForm(DeleteType::class, $media);

        if ($request->isMethod(Request::METHOD_POST)) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $mediaManager->deleteMedia($media);

                return $this->redirectToRoute('zym_media');
            }
        }

        if ($request->isMethod(Request::METHOD_DELETE)) {
            $mediaManager->deleteMedia($media);

            return $this->redirectToRoute('zym_media');
        }

        return array(
            'media' => $origMedia,
            'form' => $form->createView()
        );
    }

    /**
     * Show a media
     *
     * @param Media $media
     *
     * @Route(
     *     "/{id}.{_format}",
     *     name     ="zym_media_show",
     *     defaults = { "_format" = "html" }
     * )
     * @ParamConverter("media", class="ZymMediaBundle:Media")
     * @Template()
     *
     * @SecureParam(name="media", permissions="VIEW")
     */
    public function showAction(Media $media)
    {
        $mediaPool = $this->get('zym_media.media_pool');
        $provider  = $mediaPool->getProvider($media->getProviderName());


        return array(
            'media'   => $media,
            'formats' => $provider->getFormats()
        );
    }

    /**
     * @throws NotFoundHttpException
     *
     * @param string $id
     * @param string $format
     *
     * @return Response
     *
     * @ParamConverter("media", class="ZymMediaBundle:Media")
     * @SecureParam(name="media", permissions="VIEW")
     */
    public function viewAction(Media $media, $format = 'reference')
    {
        return $this->render('ZymMediaBundle:Media:view.html.twig', array(
            'media'     => $media,
            'formats'   => $this->get('sonata.media.pool')->getFormatNamesByContext($media->getContext()),
            'format'    => $format
        ));
    }

    /**
     * @throws NotFoundHttpException
     *
     * @param Media $media
     * @param string $format
     *
     * @return Response
     *
     * @ParamConverter("media", class="ZymMediaBundle:Media")
     * @SecureParam(name="media", permissions="VIEW")
     */
    public function downloadAction(Media $media, $format = 'reference')
    {
        /** @var MediaPool  */
        $mediaPool = $this->get('zym_media.media_pool');
        $provider  = $mediaPool->getProvider($media->getProviderName());

        $response = $provider->getDownloadResponse($media, $format, $mediaPool->getDownloadMode($media));

        return $response;
    }
}
