<?php

/**
 * Zym Framework
 *
 * This file is part of the Zym package.
 *
 * @link      https://github.com/geoffreytran/zym for the canonical source repository
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3 License
 */

namespace Zym\Bundle\MediaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Zym\Bundle\MediaBundle\Form\ProviderDataTransformer;
use Zym\Bundle\MediaBundle\MediaPool;

class MediaType extends AbstractType
{
    /**
     * @var MediaPool
     */
    private $mediaPool;

    /**
     * @var string
     */
    private $class;

    /**
     * @param MediaPool $pool
     * @param string    $class
     */
    public function __construct(MediaPool $mediaPool, $class)
    {
        $this->mediaPool  = $mediaPool;
        $this->class = $class;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'help_block' => 'Name to call this item.',
                'attr' => [
                    'class' => 'input-block-level',
                ],
            ])
            ->add('description', TextareaType::class, [
                'required' => false,
                'help_block' => 'A short description of the item.',
                'attr' => [
                    'class' => 'input-block-level',
                ],
            ])
            ->add('authorName', TextType::class, [
                'required' => false,
                'attr' => [
                    'class' => 'input-block-level',
                ],
            ])
            ->add('copyright', TextType::class, [
                'required' => false,
                'attr' => [
                    'class' => 'input-block-level',
                ],
            ])
            ->addModelTransformer(new ProviderDataTransformer($this->mediaPool, [
                'provider' => $options['provider'],
                'context' => $options['context'],
            ]))
        ;

        $this->mediaPool->getProvider($options['provider'])->buildMediaType($builder);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => $this->class,
            'provider' => null,
            'context' => null,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return '';
    }
}
