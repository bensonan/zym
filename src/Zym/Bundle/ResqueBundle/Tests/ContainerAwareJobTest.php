<?php

/**
 * Zym Framework
 *
 * This file is part of the Zym package.
 *
 * @link      https://github.com/geoffreytran/zym for the canonical source repository
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3 License
 */

namespace Zym\Bundle\ResqueBundle\Tests;

use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpKernel\KernelInterface;

class ContainerAwareJobTest extends TestCase
{
    const ENV = 'test';
    const DEBUG = true;

    public function testGetKernel()
    {
        $job = new ContainerAwareJob();

        $this->assertNull($job->getKernel());

        $kernel = $this->createMock(KernelInterface::class);

        $job->setKernel($kernel);

        $this->assertSame($kernel, $job->getKernel());
    }

    public function testSetKernelOptions()
    {
        $job = new ContainerAwareJob();

        $job->args = ['test' => true];

        $job->setKernelOptions([
            'test2' => true,
        ]);

        $this->assertEquals(['test' => true, 'test2' => true], $job->args);
    }

    public function testGetContainerCreatesKernel()
    {
        $job = new ContainerAwareJob();

        $job->setKernelOptions([
            'kernel.root_dir' => __DIR__ . '/app',
        ]);

        $job->run([]);

        $this->assertInstanceOf(KernelInterface::class, $job->getKernel());
    }

    public function testGetContainerCreatesKernelWithEnvAndDebugNotSpecified()
    {
        $job = new ContainerAwareJob();

        $job->setKernelOptions([
            'kernel.root_dir' => __DIR__ . '/app',
        ]);

        $job->run();

        $this->assertEquals('prod', $job->getKernel()->getEnvironment());
        $this->assertFalse($job->getKernel()->isDebug());
    }

    public function testGetContainerCreatesKernelWithEnvAndDebugSpecified()
    {
        $job = new ContainerAwareJob();

        $job->setKernelOptions([
            'kernel.root_dir'    => __DIR__ . '/app',
            'kernel.environment' => self::ENV,
            'kernel.debug'       => self::DEBUG,
        ]);

        $job->run([]);

        $this->assertEquals('test', $job->getKernel()->getEnvironment());
        $this->assertTrue($job->getKernel()->isDebug());
    }

    public function testGetContainerUsesExistingKernel()
    {
        $job = new ContainerAwareJob();

        $job->setKernelOptions([
            'kernel.root_dir' => __DIR__ . '/app',
        ]);

        $kernel = $this->createMock(KernelInterface::class);

        $job->setKernel($kernel);

        $job->run();

        $this->assertEquals($kernel, $job->getKernel());
    }

    public function testTearDownWithoutKernel()
    {
        $job = new ContainerAwareJob();

        $this->assertNull($job->getKernel());

        $job->tearDown();
    }

    public function testTearDownShutsdownKernel()
    {
        $job = new ContainerAwareJob();

        $job = $job;

        $kernel = $this->createMock(KernelInterface::class);

        $kernel->expects($this->once())
            ->method('shutdown');

        $job->setKernel($kernel);

        $job->tearDown();
    }

    public function tearDown()
    {
        $varDir = __DIR__ . '/var';

        if (file_exists($varDir)) {
            $this->rrmdir($varDir);
        }
    }

    private function rrmdir($dir)
    {
        foreach (glob($dir . '/*') as $file) {
            if (is_dir($file)) {
                $this->rrmdir($file);
            } else {
                unlink($file);
            }
        }

        rmdir($dir);
    }
}
