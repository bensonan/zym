<?php

use Symfony\Component\Config\Loader\LoaderInterface;
use Zym\HttpKernel\Kernel;

class ResqueKernel extends Kernel
{
    /**
     * {@inheritdoc}
     */
    public function registerBundles()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function registerContainerConfiguration(LoaderInterface $loader)
    {
    }
}
