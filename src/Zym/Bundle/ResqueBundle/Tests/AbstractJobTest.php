<?php

/**
 * Zym Framework
 *
 * This file is part of the Zym package.
 *
 * @link      https://github.com/geoffreytran/zym for the canonical source repository
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3 License
 */

namespace Zym\Bundle\ResqueBundle\Tests;

use PHPUnit\Framework\TestCase;
use Zym\Bundle\ResqueBundle\AbstractJob;
use Zym\Bundle\ResqueBundle\Failure\Redis;

class AbstractJobTest extends TestCase
{
    /**
     * @var MockJob
     */
    private $job;

    protected function setUp()
    {
        $this->job = new MockJob();
    }

    public function testConstructorSetsQueue()
    {
        $this->assertEquals('default', $this->job->queue);
    }

    public function testGetName()
    {
        $this->assertEquals(MockJob::class, $this->job->getName());
    }

    public function testPerformSetsFailureBackend()
    {
        $this->job->perform();
        $this->assertEquals(Redis::class, \Resque_Failure::getBackend());
    }

    public function testPerformCallsRun()
    {
        $job = $this->getMockBuilder(AbstractJob::class)
            ->getMockForAbstractClass();

        $job->expects($this->once())
            ->method('run');

        $job->perform();
    }
}
