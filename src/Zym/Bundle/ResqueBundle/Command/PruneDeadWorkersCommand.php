<?php

/**
 * Zym Framework
 *
 * This file is part of the Zym package.
 *
 * @link      https://github.com/geoffreytran/zym for the canonical source repository
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3 License
 */

namespace Zym\Bundle\ResqueBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Zym\Bundle\ResqueBundle\Resque;

class PruneDeadWorkersCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('zym:resque:prune-dead-workers')
            ->setDescription('Prune dead workers')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();

        $output->writeln('<info>Resque</info>');
        $output->writeln('');
        $output->writeln('Pruning dead workers');

        /** @var Resque $resque */
        $resque = $container->get('zym_resque.resque');
        $resque->pruneDeadWorkers();
    }
}
