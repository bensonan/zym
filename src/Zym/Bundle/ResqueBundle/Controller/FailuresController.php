<?php

/**
 * Zym Framework
 *
 * This file is part of the Zym package.
 *
 * @link      https://github.com/geoffreytran/zym for the canonical source repository
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3 License
 */

namespace Zym\Bundle\ResqueBundle\Controller;

use JMS\SecurityExtraBundle\Annotation\Secure;
use JMS\SecurityExtraBundle\Annotation\SecureParam;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Zym\Bundle\ResqueBundle\Failure\Redis as FailedJobs;
use Zym\Bundle\ResqueBundle\Resque;

class FailuresController extends Controller
{
    /**
     * @Route(
     *     ".{_format}",
     *     name="zym_resque_failures",
     *     defaults={
     *         "_format" = "html"
     *     },
     *     options={ "expose" = "true" }
     * )
     * @Template()
     *
     * @param Request $request
     */
    public function indexAction(Request $request)
    {
        $page     = $request->query->get('page', 1);
        $limit    = $request->query->get('limit', 50);
        $orderBy  = $request->query->get('orderBy');
        $filterBy = $request->query->get('filterBy');

        /** @var Resque $resque */
        $resque = $this->get('zym_resque.resque');

        $start = FailedJobs::count() - ($page * $limit);
        $failures = array_reverse(FailedJobs::all($start < 0 ? 0 : $start, $start < 0 ? $limit - abs($start) : $limit));

        return [
            'resque'   => $resque,
            'failures' => $failures,
            'page'     => (int)$page,
            'limit'    => (int)$limit,
            'count'    => FailedJobs::count(),
        ];
    }
}
