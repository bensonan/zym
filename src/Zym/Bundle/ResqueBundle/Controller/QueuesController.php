<?php

/**
 * Zym Framework
 *
 * This file is part of the Zym package.
 *
 * @link      https://github.com/geoffreytran/zym for the canonical source repository
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3 License
 */

namespace Zym\Bundle\ResqueBundle\Controller;

use FOS\RestBundle\Controller\Annotations\View;
use JMS\SecurityExtraBundle\Annotation\Secure;
use JMS\SecurityExtraBundle\Annotation\SecureParam;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Zym\Bundle\ResqueBundle\Resque;

class QueuesController extends Controller
{
    /**
     * @Route(
     *    ".{_format}",
     *    name="zym_resque_queues",
     *    defaults={ "_format" = "html" },
     *    options={ "expose"="true" }
     * )
     * @View
     */
    public function indexAction()
    {
        /** @var Resque $resque */
        $resque = $this->get('zym_resque.resque');
        $queues = $resque->getQueues();

        return [
            'queues' => $queues,
        ];
    }

    /**
     * @Route(
     *    "{queue}/jobs.{_format}",
     *    name="zym_resque_queues_jobs",
     *    defaults={ "_format" = "html" },
     *    options={ "expose"="true" }
     * )
     * @View
     */
    public function jobsAction(Request $request)
    {
        $queueName = $request->get('queue');

        $page     = (int)$request->query->get('page', 1);
        $limit    = (int)$request->query->get('limit', 50);
        $orderBy  = $request->query->get('orderBy');
        $filterBy = $request->query->get('filterBy');

        /** @var Resque $resque */
        $resque  = $this->get('zym_resque.resque');
        $queue = $resque->getQueue($queueName);

        $count = $queue->countJobs();
        $start = $count - ($page * $limit);
        $jobs  = $queue->getJobs($start < 0 ? 0 : $start, $start < 0 ? $limit - abs($start) : $limit);

        return [
            'jobs' => [
                'pageIndex'    => (int)$page,
                'itemsPerPage' => (int)$limit,
                'items'        => $jobs,
                'totalItems'   => (int)$count,
            ],
        ];
    }
}
