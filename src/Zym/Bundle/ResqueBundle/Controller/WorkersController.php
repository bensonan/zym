<?php

/**
 * Zym Framework
 *
 * This file is part of the Zym package.
 *
 * @link      https://github.com/geoffreytran/zym for the canonical source repository
 * @copyright Copyright (c) 2014 Geoffrey Tran <geoffrey.tran@gmail.com>
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3 License
 */

namespace Zym\Bundle\ResqueBundle\Controller;

use FOS\RestBundle\Controller\Annotations\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Zym\Bundle\ResqueBundle\Resque;

class WorkersController extends Controller
{
    /**
     * @Route(
     *    ".{_format}",
     *    name="zym_resque_workers",
     *    defaults={ "_format" = "html" },
     *    options={ "expose"="true" }
     * )
     * @View
     */
    public function indexAction()
    {
        /** @var Resque $resque */
        $resque  = $this->get('zym_resque.resque');
        $resque->pruneDeadWorkers();

        $workers = $resque->getWorkers();

        return [
            'workers' => $workers,
        ];
    }
}
